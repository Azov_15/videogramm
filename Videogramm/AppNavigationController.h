//
//  AppNavigationController.h
//  Videogramm
//
//  Created by Vladimir Azov on 02/06/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppNavigationController : UINavigationController

@end
