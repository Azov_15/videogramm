//
//  AppNavigationController.m
//  Videogramm
//
//  Created by Vladimir Azov on 02/06/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "AppNavigationController.h"
#import "ViewController.h"
#import "TopicListViewController.h"

@implementation AppNavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([self.topViewController isKindOfClass:[ViewController class]] ||
        [self.topViewController isKindOfClass:[TopicListViewController class]])
    {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAll;
}

@end
