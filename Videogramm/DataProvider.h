//
//  DataProvider.h
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PublicEnum.h"

typedef void(^SuccessBlock)(id responseObject);
typedef void(^FailureBlock)(NSError *error);

@interface DataProvider : NSObject

- (void)getPlayListByType:(PlaylistType)type
        withNextPageToken:(NSString *)nextPageToken
         withSuccessBlock:(SuccessBlock)success
              withFailure:(FailureBlock)failure;

@end
