//
//  DataProvider.m
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "DataProvider.h"
#import <AFNetworking.h>

static NSString *const apiKey = @"AIzaSyBeobmGgHZzAPO4zkg4FM5Sj66NJ5D2UjM";

@interface DataProvider ()

- (NSString *)getPlaylistIdByType:(PlaylistType)type;

@end

@implementation DataProvider

- (void)getPlayListByType:(PlaylistType)type
        withNextPageToken:(NSString *)nextPageToken
         withSuccessBlock:(SuccessBlock)success
              withFailure:(FailureBlock)failure;
{
    NSMutableString *url = [NSMutableString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=%@&maxResults=5&key=%@", [self getPlaylistIdByType:type], apiKey];
    
    if (nextPageToken.length > 0)
    {
        [url appendFormat:@"&pageToken=%@", nextPageToken];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             if ([responseObject isKindOfClass:[NSDictionary class]])
             {
                 success(responseObject);
             }
             else
             {
                 failure([NSError errorWithDomain:@"Incorrect response format" code:0 userInfo:nil]);
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             failure(error);
         }];
}

- (NSString *)getPlaylistIdByType:(PlaylistType)type
{
    NSString *playListId;
    switch (type) {
        case JoRickRevazovPlaylistType:
        {
            playListId = @"PLmkQerRlzy64jv8XCKqxlYrM0FxLhHs_r";
            break;
        }
        case RamblerPlaylistType:
        {
            playListId = @"PL7JJcdGH5aCFg8XcsyNfxL4mSOv9lBw5d";
            break;
        }
        case ApolloBrownPlaylistType:
        {
            playListId = @"PL4F32B5E3DF3E45A3";
            break;
        }
        case KirkorovPlaylistType:
        {
            playListId = @"PL5BF2AF304F0653B2";
            break;
        }
        default:
            playListId = @"";
            break;
    }
    
    return playListId;
}

@end
