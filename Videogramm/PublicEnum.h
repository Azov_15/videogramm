//
//  PublicEnum.h
//  Videogramm
//
//  Created by Vladimir Azov on 02/06/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#ifndef PublicEnum_h
#define PublicEnum_h

typedef enum : NSUInteger {
    JoRickRevazovPlaylistType,
    RamblerPlaylistType,
    ApolloBrownPlaylistType,
    KirkorovPlaylistType
} PlaylistType;

#endif /* PublicEnum_h */
