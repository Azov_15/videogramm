//
//  TopicListViewController.h
//  Videogramm
//
//  Created by Vladimir Azov on 02/06/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublicEnum.h"

@protocol TopicListViewControllerDelegate <NSObject>

- (void)didSelectValue:(PlaylistType)type;

@end

@interface TopicListViewController : UIViewController

@property (nonatomic, weak, nullable) id <TopicListViewControllerDelegate> delegate;

@end
