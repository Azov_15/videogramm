//
//  TopicListViewController.m
//  Videogramm
//
//  Created by Vladimir Azov on 02/06/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "TopicListViewController.h"

@interface TopicListViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *topicListPicker;
@property (strong, nonatomic) NSArray *pickerData;

@end

@implementation TopicListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pickerData = [NSArray arrayWithObjects:@"JoRick Revazov", @"Rambler", @"Apollo Brown", @"KIRKOROV!!", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.pickerData.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.pickerData[row];
}

- (IBAction)selectButtonClicked:(id)sender
{
    if (self.delegate)
    {
        [self.delegate didSelectValue:[self.topicListPicker selectedRowInComponent:0]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelbuttonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
