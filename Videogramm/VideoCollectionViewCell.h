//
//  VideoCollectionViewCell.h
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoModel;

@interface VideoCollectionViewCell : UICollectionViewCell

- (void)setupWithModel:(VideoModel *)model;

@end
