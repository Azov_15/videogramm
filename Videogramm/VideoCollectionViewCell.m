//
//  VideoCollectionViewCell.m
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "VideoCollectionViewCell.h"
#import "VideoModel.h"

@interface VideoCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UILabel *titleVideo;

@end

@implementation VideoCollectionViewCell

- (void)setupWithModel:(VideoModel *)model
{
    [self.coverImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.coverUrl]]]];
    [self.titleVideo setText:model.title];
}

@end
