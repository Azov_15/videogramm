//
//  VideoModel.h
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject

@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) NSString *coverUrl;
@property (nonatomic, strong) NSString *title;

- (instancetype)initWithData:(NSDictionary *)data;

@end
