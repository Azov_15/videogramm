//
//  VideoModel.m
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super init];
    if (self)
    {
        self.title = [[data objectForKey:@"snippet"] objectForKey:@"title"];
        self.coverUrl = [[[[data objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"medium"] objectForKey:@"url"];
        self.videoId = [[[data objectForKey:@"snippet"] objectForKey:@"resourceId"] objectForKey:@"videoId"];;
    }
    
    return self;
}

@end
