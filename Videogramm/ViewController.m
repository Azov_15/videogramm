//
//  ViewController.m
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import "ViewController.h"
#import "DataProvider.h"
#import "VideoCollectionViewCell.h"
#import "VideoModel.h"
#import "YTPlayerView.h"
#import "TopicListViewController.h"
#import <MBProgressHUD.h>

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, TopicListViewControllerDelegate, YTPlayerViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *videoCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseButtonTopConstraint;

@property (nonatomic, strong) DataProvider *provider;
@property (nonatomic,strong) NSMutableArray *videoData;
@property (nonatomic, strong) NSString *nextPageToken;
@property (nonatomic, assign) PlaylistType currentPlaylistType;

- (void)showErrorMessageWithTitle:(NSString *)title;
- (void)parseResponseData:(NSDictionary *)responseData;
- (void)hideYouTubeView;
- (void)animateChooseButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.provider = [DataProvider new];
    self.videoData = [NSMutableArray array];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideYouTubeView)
                                                 name:UIWindowDidBecomeHiddenNotification
                                               object:self.view.window];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)chooseTheTopic:(id)sender
{
    [self performSegueWithIdentifier:@"show_topic_list" sender:self];
}

#pragma mark - colection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.videoData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ident = @"cell";
    
    VideoCollectionViewCell *cell = (VideoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ident forIndexPath:indexPath];

    [cell setupWithModel:[self.videoData objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController * videoViewController = [[UIViewController alloc] init];
    videoViewController.view = [YTPlayerView new];
    [(YTPlayerView *)videoViewController.view setDelegate:self];
    [(YTPlayerView *)videoViewController.view loadWithVideoId:[[self.videoData objectAtIndex:indexPath.row] videoId]];
    [[MBProgressHUD showHUDAddedTo:videoViewController.view animated:YES] setLabelText:@"Loading..."];
    [self.navigationController pushViewController:videoViewController animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height && self.nextPageToken.length > 0)
    {
        [[MBProgressHUD showHUDAddedTo:self.view animated:YES] setLabelText:@"Fetching data..."];
        [self.provider getPlayListByType:self.currentPlaylistType
                       withNextPageToken:self.nextPageToken
                        withSuccessBlock:^(id responseObject) {
                            [self parseResponseData:responseObject];
                            [self.videoCollectionView reloadData];
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                        }
                             withFailure:^(NSError *error) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self showErrorMessageWithTitle:error.localizedDescription];
                             }];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"show_topic_list"])
    {
        [(TopicListViewController *)segue.destinationViewController setDelegate:self];
    }
}

- (void)didSelectValue:(PlaylistType)type
{
    self.currentPlaylistType = type;
    self.videoData = [NSMutableArray array];
    [self.videoCollectionView setHidden:YES];
    [[MBProgressHUD showHUDAddedTo:self.view animated:YES] setLabelText:@"Fetching data..."];
    self.nextPageToken = @"";
    [self.provider getPlayListByType:type
        withNextPageToken:self.nextPageToken
        withSuccessBlock:^(id responseObject) {
            [self animateChooseButton];
            [self parseResponseData:responseObject];
            [self.videoCollectionView setHidden:NO];
            [self.videoCollectionView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
        withFailure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self showErrorMessageWithTitle:error.localizedDescription];
    }];
}

- (void)playerViewDidBecomeReady:(nonnull YTPlayerView *)playerView
{
    [MBProgressHUD hideHUDForView:playerView animated:YES];
}

- (void)playerView:(nonnull YTPlayerView *)playerView didChangeToState:(YTPlayerState)state
{
    if (state == kYTPlayerStateUnstarted)
    {
        [MBProgressHUD hideHUDForView:playerView animated:YES];
        [self hideYouTubeView];
        [self showErrorMessageWithTitle:@"Can not play this video"];
    }
}

- (void)playerView:(nonnull YTPlayerView *)playerView receivedError:(YTPlayerError)error
{
    [MBProgressHUD hideHUDForView:playerView animated:YES];
    [self hideYouTubeView];
    [self showErrorMessageWithTitle:@"Can not play this video"];
}

#pragma mark - private

- (void)showErrorMessageWithTitle:(NSString *)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:title
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)parseResponseData:(NSDictionary *)responseData
{
    NSArray *videoItems = [responseData objectForKey:@"items"];
    for (NSDictionary *data in videoItems)
    {
        VideoModel *model = [[VideoModel alloc] initWithData:data];
        [self.videoData addObject:model];
    }
    self.nextPageToken = [responseData objectForKey:@"nextPageToken"];
}

- (void)animateChooseButton
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.chooseButtonTopConstraint setConstant:20];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
}

- (void)hideYouTubeView
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
