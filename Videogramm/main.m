//
//  main.m
//  Videogramm
//
//  Created by Vladimir Azov on 31/05/16.
//  Copyright © 2016 Vladimir Azov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
